import { Component, OnInit, OnDestroy, Injectable, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { User } from '@/_models';
import { UserService, AuthenticationService } from '@/_services';


@Component({
  selector: 'audit',
  templateUrl: './audit.component.html',
})

export class AuditComponent implements OnInit {
    currentUser: User;
    currentUserSubscription: Subscription;
    users: any;

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService,
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    ngOnInit() {
        this.loadAuditUsers();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    private loadAuditUsers() {
        this.userService.audit(this.currentUser).pipe(first()).subscribe(users => {
            this.users = users;
        });
    }

}